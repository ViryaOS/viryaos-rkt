FROM arm64v8/alpine:3.6

COPY qemu-aarch64-static /usr/bin/qemu-aarch64-static

RUN \
    apk update && \
# rkt dependencies
    apk add git autoconf automake gcc patch musl-dev bash squashfs-tools acl-dev libacl grep gnupg file go make diffutils coreutils cpio gzip curl && \
# stage1-xen dependencies
    apk add bc jq linux-headers glide wget tar xz binutils-dev build-base libressl-dev ncurses-dev xz-dev zlib-dev libelf-dev perl busybox-static

ENTRYPOINT ["/bin/sh"]
